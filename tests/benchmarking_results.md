# Intel(R) Core(TM) i7-3930K CPU @ 3.20GHz
/proc/cpuinfo with `bugs            : cpu_meltdown spectre_v1 spectre_v2`

## Specialized:
```
Computing winsored mean... done
used min = 10740983 max = 11586278
discarded 8192 inferior elements and 8192 superior elements
sum of times = 728506836767ns
number of runs = 65536
winsored mean of times = 11116132ns or 11.116132ms
```


## Non Specialized:
```
Computing winsored mean... done
used min = 10771187 max = 11615251
discarded 8192 inferior elements and 8192 superior elements
sum of times = 728399256111ns
number of runs = 65536
winsored mean of times = 11114490ns or 11.114490ms
```


Both are between 142 and 147 clock cycles per pixel.
