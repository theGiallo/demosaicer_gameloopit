#include <stdio.h>
#include <assert.h>
#include "lodepng.h"

void
demosaic( unsigned char * pixels, unsigned w, unsigned h, unsigned char * demosaiced )
{
	#pragma pack( push, 0 )
	struct
	RGB24
	{
		unsigned char r,g,b;
	};
	#pragma pack(pop)

	RGB24 * demosaiced_rgb24 = (RGB24*)demosaiced;
	RGB24 * pixels_rgb24 = (RGB24*)pixels;

	unsigned char bayer_filter[4] = {0,1,1,2};

	unsigned idx = 0;
	{
		int x = 0, y =0;
		RGB24 * dpx = demosaiced_rgb24 + idx;
		//RGB24 *  px = pixels_rgb24 + idx;
		int r = 0,
		    g = 0,
		    b = 0;
		int rw = 0,
		    gw = 0,
		    bw = 0;
		for ( int j = 0; j != 2; ++j )
		{
			for ( int i = 0; i != 2; ++i )
			{
				int X = x + i,
				    Y = y + j;
				int I = (         X + 2 ) % 2,
				    J = (         Y + 2 ) % 2;
				unsigned char bf = bayer_filter[ J * 2 + I ];
				RGB24 * rgb = pixels_rgb24 + ( Y * w + X );
				int rwp = ( bf == 0 ? 1 : 0 );
				int gwp = ( bf == 1 ? 1 : 0 );
				int bwp = ( bf == 2 ? 1 : 0 );
				r += rgb->r * rwp;
				g += rgb->g * gwp;
				b += rgb->b * bwp;
				rw += rwp;
				gw += gwp;
				bw += bwp;
			}
		}
		dpx->r = r / float(rw);
		dpx->g = g / float(gw);
		dpx->b = b / float(bw);
		++idx;
	}
	for ( int x = 1, y = 0; x != int(w - 1); ++x, ++idx )
	{
		RGB24 * dpx = demosaiced_rgb24 + idx;
		//RGB24 *  px = pixels_rgb24 + idx;
		int r = 0,
		    g = 0,
		    b = 0;
		int rw = 0,
		    gw = 0,
		    bw = 0;
		for ( int j = 0; j != 2; ++j )
		{
			for ( int i = -1; i != 2; ++i )
			{
				int X = x + i,
				    Y = y + j;
				int I = (         X + 2 ) % 2,
				    J = (         Y + 2 ) % 2;
				unsigned char bf = bayer_filter[ J * 2 + I ];
				RGB24 * rgb = pixels_rgb24 + ( Y * w + X );
				int rwp = ( bf == 0 ? 1 : 0 );
				int gwp = ( bf == 1 ? 1 : 0 );
				int bwp = ( bf == 2 ? 1 : 0 );
				r += rgb->r * rwp;
				g += rgb->g * gwp;
				b += rgb->b * bwp;
				rw += rwp;
				gw += gwp;
				bw += bwp;
			}
		}
		dpx->r = r / float(rw);
		dpx->g = g / float(gw);
		dpx->b = b / float(bw);
	}
	{
		int x = w - 1, y = 0;
		RGB24 * dpx = demosaiced_rgb24 + idx;
		//RGB24 *  px = pixels_rgb24 + idx;
		int r = 0,
		    g = 0,
		    b = 0;
		int rw = 0,
		    gw = 0,
		    bw = 0;
		for ( int j = 0; j != 2; ++j )
		{
			for ( int i = -1; i != 1; ++i )
			{
				int X = x + i,
				    Y = y + j;
				int I = (         X + 2 ) % 2,
				    J = (         Y + 2 ) % 2;
				unsigned char bf = bayer_filter[ J * 2 + I ];
				RGB24 * rgb = pixels_rgb24 + ( Y * w + X );
				int rwp = ( bf == 0 ? 1 : 0 );
				int gwp = ( bf == 1 ? 1 : 0 );
				int bwp = ( bf == 2 ? 1 : 0 );
				r += rgb->r * rwp;
				g += rgb->g * gwp;
				b += rgb->b * bwp;
				rw += rwp;
				gw += gwp;
				bw += bwp;
			}
		}
		dpx->r = r / float(rw);
		dpx->g = g / float(gw);
		dpx->b = b / float(bw);
		++idx;
	}
	for ( int y = 1; y != int(h - 1); ++y )
	{
		int x = 0;

		{
			RGB24 * dpx = demosaiced_rgb24 + idx;
			//RGB24 *  px = pixels_rgb24 + idx;
			int r = 0,
			    g = 0,
			    b = 0;
			int rw = 0,
			    gw = 0,
			    bw = 0;
			for ( int j = -1; j != 2; ++j )
			{
				for ( int i = 0; i != 2; ++i )
				{
					int X = x + i,
					    Y = y + j;
					int I = (         X + 2 ) % 2,
					    J = (         Y + 2 ) % 2;
					unsigned char bf = bayer_filter[ J * 2 + I ];
					RGB24 * rgb = pixels_rgb24 + ( Y * w + X );
					int rwp = ( bf == 0 ? 1 : 0 );
					int gwp = ( bf == 1 ? 1 : 0 );
					int bwp = ( bf == 2 ? 1 : 0 );
					r += rgb->r * rwp;
					g += rgb->g * gwp;
					b += rgb->b * bwp;
					rw += rwp;
					gw += gwp;
					bw += bwp;
				}
			}
			dpx->r = r / float(rw);
			dpx->g = g / float(gw);
			dpx->b = b / float(bw);
			++idx;
			++x;
		}
		for ( /*x = 1*/; x != int(w - 1); ++x, ++idx )
		{
			RGB24 * dpx = demosaiced_rgb24 + idx;
			//RGB24 *  px = pixels_rgb24 + idx;
			int r = 0,
			    g = 0,
			    b = 0;
			int rw = 0,
			    gw = 0,
			    bw = 0;
			#if DEBUG
			//if ( x == 2 ) abort();
			if ( h - 1 - y < 4 && x < 4 )
			{
				printf( "%d,%d\n", x, y );
			}
			#endif
			for ( int j = -1; j != 2; ++j )
			{
				for ( int i = -1; i != 2; ++i )
				{
					int X = x + i,
					    Y = y + j;
					int I = (         X + 2 ) % 2,
					    J = (         Y + 2 ) % 2;
					unsigned char bf = bayer_filter[ J * 2 + I ];
					RGB24 * rgb = pixels_rgb24 + ( Y * w + X );
					int rwp = ( bf == 0 ? 1 : 0 );
					int gwp = ( bf == 1 ? 1 : 0 );
					int bwp = ( bf == 2 ? 1 : 0 );
					r += rgb->r * rwp;
					g += rgb->g * gwp;
					b += rgb->b * bwp;
					rw += rwp;
					gw += gwp;
					bw += bwp;
					#if DEBUG
					if ( h - 1 - y < 4 && x < 4 )
					{
						printf( "    %d,%d(%d) %d,%d wp = %d %d %d\n", X, Y, h - 1 - Y, I, J, rwp, gwp, bwp );
					}
					#endif
				}
			}
			dpx->r = r / float(rw);
			dpx->g = g / float(gw);
			dpx->b = b / float(bw);
			#if DEBUG
			if ( h - 1 - y < 4 && x < 4 )
			{
				printf( "    x, h - 1 -y = %3d %3d\n", x, h - 1 - y );
				printf( "       w = %3d %3d %3d\n", rw, gw, bw );
				printf( "    rgbw = %3d %3d %3d\n", r, g, b );
				printf( "    rgb  = %3d %3d %3d\n", dpx->r, dpx->g, dpx->b );
			}
			#endif
		}
		{
			RGB24 * dpx = demosaiced_rgb24 + idx;
			RGB24 *  px = pixels_rgb24 + idx;
			dpx = demosaiced_rgb24 + idx;
			 px = pixels_rgb24 + idx;
			int r = 0,
			    g = 0,
			    b = 0;
			int rw = 0,
			    gw = 0,
			    bw = 0;
			for ( int j = -1; j != 2; ++j )
			{
				for ( int i = -1; i != 1; ++i )
				{
					int X = x + i,
					    Y = y + j;
					int I = (         X + 2 ) % 2,
					    J = (         Y + 2 ) % 2;
					unsigned char bf = bayer_filter[ J * 2 + I ];
					RGB24 * rgb = pixels_rgb24 + ( Y * w + X );
					int rwp = ( bf == 0 ? 1 : 0 );
					int gwp = ( bf == 1 ? 1 : 0 );
					int bwp = ( bf == 2 ? 1 : 0 );
					r += rgb->r * rwp;
					g += rgb->g * gwp;
					b += rgb->b * bwp;
					rw += rwp;
					gw += gwp;
					bw += bwp;
				}
			}
			dpx->r = r / float(rw);
			dpx->g = g / float(gw);
			dpx->b = b / float(bw);
			++idx;
		}
	}
	{
		int x = 0, y = h - 1;
		RGB24 * dpx = demosaiced_rgb24 + idx;
		//RGB24 *  px = pixels_rgb24 + idx;
		int r = 0,
		    g = 0,
		    b = 0;
		int rw = 0,
		    gw = 0,
		    bw = 0;
		for ( int j = -1; j != 1; ++j )
		{
			for ( int i = 0; i != 2; ++i )
			{
				int X = x + i,
				    Y = y + j;
				int I = (         X + 2 ) % 2,
				    J = (         Y + 2 ) % 2;
				unsigned char bf = bayer_filter[ J * 2 + I ];
				RGB24 * rgb = pixels_rgb24 + ( Y * w + X );
				int rwp = ( bf == 0 ? 1 : 0 );
				int gwp = ( bf == 1 ? 1 : 0 );
				int bwp = ( bf == 2 ? 1 : 0 );
				r += rgb->r * rwp;
				g += rgb->g * gwp;
				b += rgb->b * bwp;
				rw += rwp;
				gw += gwp;
				bw += bwp;
			}
		}
		dpx->r = r / float(rw);
		dpx->g = g / float(gw);
		dpx->b = b / float(bw);
		++idx;
	}
	for ( int x = 1, y = h - 1; x != int(w - 1); ++x, ++idx )
	{
		RGB24 * dpx = demosaiced_rgb24 + idx;
		//RGB24 *  px = pixels_rgb24 + idx;
		int r = 0,
		    g = 0,
		    b = 0;
		int rw = 0,
		    gw = 0,
		    bw = 0;
		for ( int j = -1; j != 1; ++j )
		{
			for ( int i = -1; i != 2; ++i )
			{
				int X = x + i,
				    Y = y + j;
				int I = (         X + 2 ) % 2,
				    J = (         Y + 2 ) % 2;
				unsigned char bf = bayer_filter[ J * 2 + I ];
				RGB24 * rgb = pixels_rgb24 + ( Y * w + X );
				int rwp = ( bf == 0 ? 1 : 0 );
				int gwp = ( bf == 1 ? 1 : 0 );
				int bwp = ( bf == 2 ? 1 : 0 );
				r += rgb->r * rwp;
				g += rgb->g * gwp;
				b += rgb->b * bwp;
				rw += rwp;
				gw += gwp;
				bw += bwp;
			}
		}
		dpx->r = r / float(rw);
		dpx->g = g / float(gw);
		dpx->b = b / float(bw);
	}
	{
		int x = w - 1, y = h - 1;
		RGB24 * dpx = demosaiced_rgb24 + idx;
		//RGB24 *  px = pixels_rgb24 + idx;
		int r = 0,
		    g = 0,
		    b = 0;
		int rw = 0,
		    gw = 0,
		    bw = 0;
		for ( int j = -1; j != 1; ++j )
		{
			for ( int i = -1; i != 1; ++i )
			{
				int X = x + i,
				    Y = y + j;
				int I = (         X + 2 ) % 2,
				    J = (         Y + 2 ) % 2;
				unsigned char bf = bayer_filter[ J * 2 + I ];
				RGB24 * rgb = pixels_rgb24 + ( Y * w + X );
				int rwp = ( bf == 0 ? 1 : 0 );
				int gwp = ( bf == 1 ? 1 : 0 );
				int bwp = ( bf == 2 ? 1 : 0 );
				r += rgb->r * rwp;
				g += rgb->g * gwp;
				b += rgb->b * bwp;
				rw += rwp;
				gw += gwp;
				bw += bwp;
			}
		}
		dpx->r = r / float(rw);
		dpx->g = g / float(gw);
		dpx->b = b / float(bw);
		++idx;
	}
}
void
demosaic( char * file_path_png )
{
	#if DEBUG
	printf( "%s\n", file_path_png );
	#endif
	unsigned char * pixels;
	unsigned w = 0, h = 0;
	unsigned error = lodepng_decode24_file( &pixels, &w, &h, file_path_png );
	if ( error )
	{
		printf( "error loading '%s': %s\n", file_path_png, lodepng_error_text( error ) );
		return;
	}
	#if DEBUG
	printf( "w = %u h = %u\n", w, h );
	#endif
	if ( w == 0 || h == 0 )
	{
		printf( "image has one or more dimensions equal to 0: %u x %u\n", w, h );
		return;
	}

	unsigned char * demosaiced = (unsigned char*)malloc( w * h * 24 );

	demosaic( pixels, w, h, demosaiced );

	char out_file_path[1024] = {};
	{
		int l;
		for ( l = 0; l != sizeof( out_file_path ) && file_path_png[l] != 0; ++l )
		{
			out_file_path[l] = file_path_png[l];
		}
		char postfix[] = "_demosaiced.png";
		int start = l;
		if ( out_file_path[ l - 4 ] == '.' )
		{
			start = l - 4;
		}
		for ( int i = 0; i != sizeof( postfix ); ++i )
		{
			out_file_path[ start + i ] = postfix[i];
		}
	}
	error = lodepng_encode24_file( out_file_path, demosaiced, w, h );
	if ( error )
	{
		printf( "got error while encoding output '%s': %s", out_file_path, lodepng_error_text( error ) );
	}

	free( demosaiced );
	free( pixels );
}

void
demosaic_dyn( unsigned char * pixels, unsigned w, unsigned h, unsigned char * demosaiced )
{
	#pragma pack( push, 0 )
	struct
	RGB24
	{
		unsigned char r,g,b;
	};
	#pragma pack(pop)

	RGB24 * demosaiced_rgb24 = (RGB24*)demosaiced;
	RGB24 * pixels_rgb24 = (RGB24*)pixels;

	unsigned char bayer_filter[4] = {0,1,1,2};

	unsigned idx = 0;
	for ( int y = 0; y != int(h); ++y )
	{
		for ( int x = 0; x != int(w); ++x )
		{
			RGB24 * dpx = demosaiced_rgb24 + idx;
			RGB24 stub_px = {};
			//RGB24 *  px = pixels_rgb24 + idx;
			int r = 0,
			    g = 0,
			    b = 0;
			int rw = 0,
			    gw = 0,
			    bw = 0;
			for ( int j = -1; j != 2; ++j )
			{
				for ( int i = -1; i != 2; ++i )
				{
					int X = x + i,
					    Y = y + j;
					int I = ( X + 2 ) % 2,
					    J = ( Y + 2 ) % 2;
					bool is_out = X < 0 || X >= int(w) || Y < 0 || Y >= int(h);
					unsigned char bf = bayer_filter[ J * 2 + I ];
					RGB24 * rgb = is_out ? &stub_px : pixels_rgb24 + ( Y * w + X );
					int rwp = ( !is_out && bf == 0 ? 1 : 0 );
					int gwp = ( !is_out && bf == 1 ? 1 : 0 );
					int bwp = ( !is_out && bf == 2 ? 1 : 0 );
					r += rgb->r * rwp;
					g += rgb->g * gwp;
					b += rgb->b * bwp;
					rw += rwp;
					gw += gwp;
					bw += bwp;
				}
			}
			dpx->r = r / float(rw);
			dpx->g = g / float(gw);
			dpx->b = b / float(bw);
			++idx;
		}
	}
	assert( idx == w * h );
}
void
demosaic_dyn( char * file_path_png )
{
	#if DEBUG
	printf( "%s\n", file_path_png );
	#endif
	unsigned char * pixels;
	unsigned w = 0, h = 0;
	unsigned error = lodepng_decode24_file( &pixels, &w, &h, file_path_png );
	if ( error )
	{
		printf( "error loading '%s': %s\n", file_path_png, lodepng_error_text( error ) );
		return;
	}
	#if DEBUG
	printf( "w = %u h = %u\n", w, h );
	#endif
	if ( w == 0 || h == 0 )
	{
		printf( "image has one or more dimensions equal to 0: %u x %u\n", w, h );
		return;
	}

	unsigned char * demosaiced = (unsigned char*)malloc( w * h * 24 );

	demosaic_dyn( pixels, w, h, demosaiced );

	char out_file_path[1024] = {};
	{
		int l;
		for ( l = 0; l != sizeof( out_file_path ) && file_path_png[l] != 0; ++l )
		{
			out_file_path[l] = file_path_png[l];
		}
		char postfix[] = "_demosaiced.png";
		int start = l;
		if ( out_file_path[ l - 4 ] == '.' )
		{
			start = l - 4;
		}
		for ( int i = 0; i != sizeof( postfix ); ++i )
		{
			out_file_path[ start + i ] = postfix[i];
		}
	}
	error = lodepng_encode24_file( out_file_path, demosaiced, w, h );
	if ( error )
	{
		printf( "got error while encoding output '%s': %s", out_file_path, lodepng_error_text( error ) );
	}

	free( demosaiced );
	free( pixels );
}
inline
uint64_t
ns_time()
{
	uint64_t ret = {};

	timespec t;
	int64_t r = clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &t );
	if ( r == -1 )
	{
		fprintf( stderr, "error: clock_gettime\n" );
	} else
	{
		ret = t.tv_sec * (uint64_t)1e9 + t.tv_nsec;
	}

	return ret;
}

enum
Sort_Order : uint8_t
{
	SORT_ASCENDANTLY,
	SORT_DESCENDANTLY
};

static
void
radix_sort_mc_64_32( const uint64_t * keys, uint32_t * ids, uint32_t * tmp, uint32_t count, Sort_Order sort_order )
{
#define BYTES_COUNT 8
	uint32_t firsts[BYTES_COUNT][256] = {};

	// NOTE(theGiallo): count buckets occurrencies
	if ( sort_order == SORT_ASCENDANTLY )
	{
		for ( uint32_t i = 0; i != count; ++i )
		{
			++firsts[0][( keys[i]       ) & 0xFF];
			++firsts[1][( keys[i] >> 8  ) & 0xFF];
			++firsts[2][( keys[i] >> 16 ) & 0xFF];
			++firsts[3][( keys[i] >> 24 ) & 0xFF];
			++firsts[4][( keys[i] >> 32 ) & 0xFF];
			++firsts[5][( keys[i] >> 40 ) & 0xFF];
			++firsts[6][( keys[i] >> 48 ) & 0xFF];
			++firsts[7][( keys[i] >> 56 ) & 0xFF];
		}
	} else
	{
		for ( uint32_t i = 0; i != count; ++i )
		{
			++firsts[0][0xFF - ( ( keys[i] >> 0  ) & 0xFF )];
			++firsts[1][0xFF - ( ( keys[i] >> 8  ) & 0xFF )];
			++firsts[2][0xFF - ( ( keys[i] >> 16 ) & 0xFF )];
			++firsts[3][0xFF - ( ( keys[i] >> 24 ) & 0xFF )];
			++firsts[4][0xFF - ( ( keys[i] >> 32 ) & 0xFF )];
			++firsts[5][0xFF - ( ( keys[i] >> 40 ) & 0xFF )];
			++firsts[6][0xFF - ( ( keys[i] >> 48 ) & 0xFF )];
			++firsts[7][0xFF - ( ( keys[i] >> 56 ) & 0xFF )];
		}
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	uint32_t counts[BYTES_COUNT] = {};
	for ( uint32_t i = 0; i != 256; ++i )
	{
		uint32_t v[BYTES_COUNT];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		v[4] = firsts[4][i];
		v[5] = firsts[5][i];
		v[6] = firsts[6][i];
		v[7] = firsts[7][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		firsts[4][i] = counts[4];
		firsts[5][i] = counts[5];
		firsts[6][i] = counts[6];
		firsts[7][i] = counts[7];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
		counts[4] += v[4];
		counts[5] += v[5];
		counts[6] += v[6];
		counts[7] += v[7];
	}

	for ( uint8_t b=0; b!=BYTES_COUNT; ++b )
	{
		for ( uint32_t i = 0; i != count; ++i )
		{
			uint8_t id;
			if ( sort_order == SORT_ASCENDANTLY )
			{
				id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;
			} else
			{
				id = 0xFF - ( ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF );
			}
			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		uint32_t * swp = tmp;
		           tmp = ids;
		           ids = swp;
	}
#undef BYTES_COUNT
}


int
main( int argc, char *argv[] )
{
	//#define BENCHMARK_RUNS_COUNT (1024*1024)
	#define BENCHMARK_RUNS_COUNT 1024
	if ( argc == 1 )
	{
		printf( "%s file_to_demosaic.png [other_files.png...]\n"
		        "\n"
		        "or\n"
		        "\n"
		        "%s --benchmark [RUNS_COUNT] file_to_demosaic.png\n"
		        "    RUNS_COUNT is the number of runs that will be performed\n"
		        "               during the benchmark. Default value = %d.\n"
		        "NOTE: benchmark mode does not weight on your storage drive.\n"
		        "      It loads the image a single time and then keeps the data.\n"
		        "      Also it does not malloc every run, but a single time.\n"
		        , argv[0], argv[0], BENCHMARK_RUNS_COUNT );
		return 1;
	}

	if ( ( argc == 3 || argc == 4 ) && 0 == strcmp( argv[1], "--benchmark" ) )
	{
		int runs_count = 0;
		const char * file_path_png = argv[2];
		if ( argc == 4 )
		{
			char tmp[strlen( argv[2] )];
			if ( argc == 4 && 1 != sscanf( argv[2], "%u%s", &runs_count, tmp ) )
			{
				printf( "the argument passed as number of benchmark runs failed to be parsed (%s)\n", argv[2] );
				return 1;
			}
			file_path_png = argv[3];
		}
		if ( runs_count == 0 )
		{
			runs_count = BENCHMARK_RUNS_COUNT;
		}
		printf( "benchmarking %d runs...\n", runs_count );
		#if DEBUG
		printf( "%s\n", file_path_png );
		#endif
		unsigned char * pixels;
		unsigned w = 0, h = 0;
		unsigned error = lodepng_decode24_file( &pixels, &w, &h, file_path_png );
		if ( error )
		{
			printf( "error loading '%s': %s\n", file_path_png, lodepng_error_text( error ) );
			return 1;
		}
		#if DEBUG
		printf( "w = %u h = %u\n", w, h );
		#endif
		if ( w == 0 || h == 0 )
		{
			printf( "image has one or more dimensions equal to 0: %u x %u\n", w, h );
			return 1;
		}

		unsigned char * demosaiced = (unsigned char*)malloc( w * h * 24 );

		printf( "\nRunning test for specialized version...\n" );

		uint64_t * times = (uint64_t*) malloc( runs_count * sizeof(*times) );
		for ( int i = 0, pi = 0; i != runs_count; ++i, ++pi )
		{
			if ( pi == 0 || pi == runs_count / 100 )
			{
				pi = 0;
				printf( "%6.2f%% (%9d/%9d)\n", ( 100 * i ) / (double)runs_count, i, runs_count );
			}
			uint64_t start = ns_time();
			demosaic( pixels, w, h, demosaiced );
			uint64_t end = ns_time();
			times[i] = end - start;
		}

		printf( "\nComputing winsored mean..." );

		uint32_t * ids     = (uint32_t*) malloc( runs_count * sizeof(*ids) );
		uint32_t * tmp_ids = (uint32_t*) malloc( runs_count * sizeof(*tmp_ids) );
		for ( int i = 0; i != runs_count; ++i )
		{
			ids[i] = i;
		}
		radix_sort_mc_64_32( times, ids, tmp_ids, runs_count, Sort_Order::SORT_ASCENDANTLY );

		#define DISCARD_PERCENTAGE_HALF 0.125
		uint32_t discarded_count = runs_count * DISCARD_PERCENTAGE_HALF;
		uint64_t min_v = times[ids[discarded_count]];
		uint64_t max_v = times[ids[runs_count - discarded_count]];

		uint64_t times_sum = ( min_v + max_v ) * discarded_count;
		for ( unsigned i = discarded_count; i != runs_count - discarded_count; ++i )
		{
			//printf( "%6lu\n", times[ids[i]] );
			times_sum += times[ids[i]];
		}
		printf( " done\n" );
		printf( "used min = %lu max = %lu\n", min_v, max_v );
		printf( "discarded %u inferior elements and %d superior elements\n", discarded_count, discarded_count );
		printf( "sum of times = %luns\n", times_sum );
		printf( "number of runs = %d\n", runs_count );
		uint64_t winsored_mean = times_sum / (uint64_t)runs_count;
		printf( "winsored mean of times = %luns or %fms\n", winsored_mean, winsored_mean / 1e6 );

		printf( "\nRunning test for non specialized version...\n" );

		for ( int i = 0, pi = 0; i != runs_count; ++i, ++pi )
		{
			if ( pi == 0 || pi == runs_count / 100 )
			{
				pi = 0;
				printf( "%6.2f%% (%9d/%9d)\n", ( 100 * i ) / (double)runs_count, i, runs_count );
			}
			uint64_t start = ns_time();
			demosaic( pixels, w, h, demosaiced );
			uint64_t end = ns_time();
			times[i] = end - start;
		}

		printf( "\nComputing winsored mean..." );

		for ( int i = 0; i != runs_count; ++i )
		{
			ids[i] = i;
		}
		radix_sort_mc_64_32( times, ids, tmp_ids, runs_count, Sort_Order::SORT_ASCENDANTLY );

		//#define DISCARD_PERCENTAGE_HALF 0.125
		discarded_count = runs_count * DISCARD_PERCENTAGE_HALF;
		min_v = times[ids[discarded_count]];
		max_v = times[ids[runs_count - discarded_count]];

		times_sum = ( min_v + max_v ) * discarded_count;
		for ( unsigned i = discarded_count; i != runs_count - discarded_count; ++i )
		{
			//printf( "%6lu\n", times[ids[i]] );
			times_sum += times[ids[i]];
		}
		printf( " done\n" );
		printf( "used min = %lu max = %lu\n", min_v, max_v );
		printf( "discarded %u inferior elements and %d superior elements\n", discarded_count, discarded_count );
		printf( "sum of times = %luns\n", times_sum );
		printf( "number of runs = %d\n", runs_count );
		winsored_mean = times_sum / (uint64_t)runs_count;
		printf( "winsored mean of times = %luns or %fms\n", winsored_mean, winsored_mean / 1e6 );


		free( times );
		free( pixels );
		free( demosaiced );
	} else
	{
		for ( int i = 1; i != argc; ++i )
		{
			#if 1
			demosaic( argv[i] );
			#else
			demosaic_dyn( argv[i] );
			#endif
		}
	}
	return 0;
}

#include "lodepng.cpp"
